<?php
class Bitbull_Ordershare_Model_Configoptions {

    public function toOptionArray()
    {
        return array(
            array('value'=>1, 'label'=>Mage::helper('ordershare')->__('Share whole order only')),
            array('value'=>2, 'label'=>Mage::helper('ordershare')->__('Share single products only')),
            array('value'=>3, 'label'=>Mage::helper('ordershare')->__('Both')),
        );
    }
}