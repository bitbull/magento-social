<?php

class Bitbull_Ordershare_Model_Observer {


    public function addMetaToWishlist(Varien_Event_Observer $observer)
    {
        $controller = $observer->getEvent()->getData('action');

        if ($controller instanceof Bitbull_Ordershare_IndexController && $controller->getRequest()->getModuleName() == 'ordershare' && $controller->getRequest()->getControllerName() == 'index') {

            /* @var $update Mage_Core_Model_Layout_Update */
            $update = $observer->getEvent()->getLayout()->getUpdate();

            if ($controller->getRequest()->getActionName() == 'view') {
                $update->addHandle('wishlist_share_meta');
            } elseif ($controller->getRequest()->getActionName() == 'share') {
                $update->addHandle('order_share_meta');
            }
        }
    }

}
