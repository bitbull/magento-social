<?php
class Bitbull_Ordershare_Block_Ordershare extends Mage_Core_Block_Template
{

	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }


    /**
     * Get wishlist item collection by wishlist sharing code
     *
     * @param string $wishlistSharingCode
     * @return Mage_Wishlist_Model_Mysql4_Item_Collection
     */
    public function getWishlistEntity($wishlistSharingCode)
    {
        $wishlist = Mage::getSingleton('wishlist/wishlist')->loadByCode($wishlistSharingCode);
        //$this->isWishlistShared($wishlist);

        $wishlistItemCollection = $wishlist->getItemCollection();

        if (count($wishlistItemCollection)) {
            return $wishlistItemCollection;
        } else {
            return null;
        }
    }

    /**
     * Get wishlist public url
     *
     * @param $wishlistSharingCode
	 * @param $wishlist
     * @return string
     */
    public function getWishShareLink($wishlistSharingCode, $wishlist)
    {
        $this->isWishlistShared($wishlist);
        return $this->getUrl('ordershare/index/view', array('wish_share_code' => $wishlistSharingCode));
    }

    /**
     * Check if wishlit is shared
     *
     * @param $wishlist
     */
    public function isWishlistShared($wishlist)
    {
        if (!$wishlist->getShared()) {
            $wishlist->setShared(1);
            $wishlist->save();
        }
    }
	
	 /**
     * Get user full name from wishlist
     *
     * @param string $wishlistSharingCode
     * @return string customer full name
     */
    public function getUserDataFromWishlist($wishlistSharingCode)
    {
        $wishlist = Mage::getSingleton('wishlist/wishlist')->loadByCode($wishlistSharingCode);
        return Mage::getModel('customer/customer')->load($wishlist->getCustomerId())->getName();
    }

}