Bitbull_SocialConnect
=====================
Bitbull_SocialConnect is a Magento extension allowing your customers to login or create an account at your store using their Google, Facebook or Twitter account.